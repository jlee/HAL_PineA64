How to build U-Boot for Allwinner A64 Pinebook

Based on instructions by J Ballance, 2 May 2019

At the time of writing, upstream repositories are hosted at:
      https://github.com/ARM-software/arm-trusted-firmware.git
  and https://github.com/u-boot/u-boot.git

Snapshots of the relevant versions and patches thereto are copied here in the
form of shallow clone repositories.

To install dependency packages, unpack the git repositories and build the
binaries, simply do:

$ ./build.sh

The resulting file u-boot/mgubook should be copied to
tools.!A64ROM-InSitu and/or tools.!A64ROM-scsi0 programmer.

Building has been tested on Ubuntu, but other Debian-based distros are likely
to work too. The binaries as shipped were built with the following toolchain:

$ gcc -v
Using built-in specs.
COLLECT_GCC=gcc
COLLECT_LTO_WRAPPER=/usr/lib/gcc/x86_64-linux-gnu/8/lto-wrapper
OFFLOAD_TARGET_NAMES=nvptx-none
OFFLOAD_TARGET_DEFAULT=1
Target: x86_64-linux-gnu
Configured with: ../src/configure -v --with-pkgversion='Ubuntu 8.2.0-7ubuntu1' --with-bugurl=file:///usr/share/doc/gcc-8/README.Bugs --enable-languages=c,ada,c++,go,brig,d,fortran,objc,obj-c++ --prefix=/usr --with-gcc-major-version-only --program-suffix=-8 --program-prefix=x86_64-linux-gnu- --enable-shared --enable-linker-build-id --libexecdir=/usr/lib --without-included-gettext --enable-threads=posix --libdir=/usr/lib --enable-nls --with-sysroot=/ --enable-clocale=gnu --enable-libstdcxx-debug --enable-libstdcxx-time=yes --with-default-libstdcxx-abi=new --enable-gnu-unique-object --disable-vtable-verify --enable-libmpx --enable-plugin --enable-default-pie --with-system-zlib --with-target-system-zlib --enable-objc-gc=auto --enable-multiarch --disable-werror --with-arch-32=i686 --with-abi=m64 --with-multilib-list=m32,m64,mx32 --enable-multilib --with-tune=generic --enable-offload-targets=nvptx-none --without-cuda-driver --enable-checking=release --build=x86_64-linux-gnu --host=x86_64-linux-gnu --target=x86_64-linux-gnu
Thread model: posix
gcc version 8.2.0 (Ubuntu 8.2.0-7ubuntu1)

ROOL, 19 Jan 2022

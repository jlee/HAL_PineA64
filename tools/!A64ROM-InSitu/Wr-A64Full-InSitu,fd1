REM CDDL HEADER START
REM
REM The contents of this file are subject to the terms of the
REM Common Development and Distribution License (the "Licence").
REM You may not use this file except in compliance with the Licence.
REM
REM You can obtain a copy of the licence at
REM RiscOS/Sources/HAL/HAL_PineA64/LICENCE.
REM See the Licence for the specific language governing permissions
REM and limitations under the Licence.
REM
REM When distributing Covered Code, include this CDDL HEADER in each
REM file and include the Licence file. If applicable, add the
REM following below this CDDL HEADER, with the fields enclosed by
REM brackets "[]" replaced with your own identifying information:
REM Portions Copyright [yyyy] [name of copyright owner]
REM
REM CDDL HEADER END
REM


ON ERROR ON ERROR OFF:REPORT:PRINT"@ ";ERL:END
REM
REM JWB 20190925
REM
REM Rom image defaults to <Obey$Dir>.A64FullRom

LIBRARY "<Progjb$Dir>.ProgLib"
PROCLibInit
REM set Dev%=TRUE if you need to create roms
Dev%=FALSE

sdfsDrive%=0  :REM sdfs0
SDCardOffset% = &2000 : REM start of SDCard of image on disc


RomFile$="<Obey$Dir>.a64rom"
BootFile$="<Obey$Dir>.mguboot"
EnvFile$= "<Obey$Dir>.mguenv9f000"
EnvOffset%=&9f000
EnvSize%  =&10000

FullRom$="<Obey$Dir>.A64FullRom"

IF Dev%THEN
  IF FNFindImage<>"" THEN
    PRINT "Active build mode detected - rebuilding the full boot image"
    REM line below inserts a new romfile if available
      PROCCopyRom(RomFile$)
    PRINT "Done"
  ENDIF

  PROCMergeRom(RomFile$,BootFile$,EnvFile$,EnvOffset%,FullRom$)
ENDIF

file$=FullRom$


start%=0
o%=0
ON ERROR ON ERROR OFF:PROCError

PRINT "Using file "+file$
filesize% = FNGetFileSize(file$)-4

IF filesize%AND&1ff THEN
  filesize%=filesize%/&200
  filesize%+=1:
  filesize%=filesize%*&200
ENDIF

DIM cmosbuf% &200

bufsize%=filesize% +4
bufsize% = (bufsize% >>9) <<9
DIM buf% bufsize% +&200

DIM ckbuf% bufsize% +&200

sdfs_SectorDiscOp=&5904a
REM Firstly, read CMOS from disc, asking disc where to go...
REM.. 1 READ the cmos using OS calls
 OSCLI("*savecmos <Obey$Dir>.cmb")
 a$="*load <Obey$Dir>.cmb "+STR$~(buf%)+ " "
 OSCLI(a$)
 a%=&1
 FOR i%=0 TO238
   a%+=buf%?i%
 NEXT
 IF (buf%?239) = (a%AND&ff)THEN
   PRINT "Good CMOS found"
 ELSE
   PRINT "Bad CMOS found"
 ENDIF
 REM Re-order the cmos buffer
 FOR i%=0 TO&0c STEP4
   cmosbuf%!(i%+&0)=buf%!(i%+&f0)
 NEXT
 FOR i%=0 TO&2c STEP 4
   cmosbuf%!(i%+&10)=buf%!(i%+&c0)
 NEXT
 FOR i%=0 TO&bc
   cmosbuf%!(i%+&40)=buf%!(i%+&00)
 NEXT
 OSCLI("X rmkill SDCMOS ")
 OSCLI("X delete <Obey$Dir>.cmb")


start%=&0

PRINT "ROM Image size is &";~filesize%

REM now ensure the relevant part of the disc is locked out
PRINT  "Mapping out disc space "
  PROCMapOutDisc("SDFS",sdfsDrive%, SDCardOffset%, filesize%)

PRINT "Programming sdfs card with Boot ROM code"
SYS"OS_File",255,file$,buf%
cb%=buf%+buf%!0 +4

CMOSLOC%= cb%+(cb%!4)-buf%-4+SDCardOffset% :REM place on disc
CMOSbufstart%=cb%+(cb%!4)
PRINT ~CMOSLOC%
PRINT:PRINT "CMOS start in Disc "; ~CMOSLOC%;" Sector "; ~CMOSLOC%>>9

  count%=filesize%

  SYS"XOS_CLI","sdfs:Dismount :"+STR$(sdfsDrive%) TO r0;flags

  REM insert the cmos sector to the new image
REM SYS sdfs_SectorDiscOp,,1,(sdfsDrive%<<29)+((CMOSLOC%)>>9),cmosbuf%,&200 TO,,r1%,r2%,r3%
  FOR i%=0 TO255 STEP4
    CMOSbufstart%!i%=cmosbuf%!i%
  NEXT


  REM write out all data
 SYS sdfs_SectorDiscOp,,2,(sdfsDrive%<<29)+(SDCardOffset%>>9),buf%+4,count%

  REM read it back in to verify
 SYS sdfs_SectorDiscOp,,1,(sdfsDrive%<<29)+(SDCardOffset%>>9),ckbuf%+4,count%

 FOR i%=4 TO count%STEP4
   IF buf%!i%<>ckbuf%!i%THEN
     PRINT:PRINT "Verify error at &"+STR$~(i%-4)+" Please program again ":END
   ENDIF
 NEXT

 PRINT
 PRINT "Read back &"+STR$~(count%)+" bytes and verified correct "
 PRINT

PRINT "End disc address (sectors) &";+STR$~((((SDCardOffset%>>9)+(count%>>9))))
PRINT "End disc total size to save         &";+STR$~((((SDCardOffset%)+(count%))))

  SYS"XOS_CLI","sdfs:Dismount :"+STR$(sdfsDrive%) TO r0;flags

REM  f%=OPENOUT("<Obey$Dir>.^.PineHint")
REM  IF f%<>0 THEN
REM   BPUT#f%, "If using uboot loader you will need to type the following 3 lines in it : "
REM   BPUT#f%, " "
REM   BPUT#f%, "setenv riscos ""mmc READ 0x40080000 0x"+STR$~((SDCardOffset%+!buf%)>>9)+" 0x2800; go 0x40080000"""
REM   BPUT#f%, " "
REM   BPUT#f%, "setenv mmc_boot ""RUN riscos"""
REM   BPUT#f%, " "
REM   BPUT#f%, "env save"
REM   CLOSE#f%
REM  ENDIF
REM  *sett. <Obey$Dir>.^.PineHint text
REM  PRINT :PRINT :PRINT
REM  PRINT "Instructions for configuring uboot are in the file PineHint":PRINT
  PRINT
  PRINT
  PRINT TAB(20),"Please reboot your machine immediately" :PRINT :PRINT
  END

DEF PROCError
   REPORT
   PRINT " @ ";ERL
   IF(o%)THEN
     SYS"OS_Find",0,o%
   ENDIF
   IF f% THEN
     CLOSE#f%
   ENDIF
   END
ENDPROC

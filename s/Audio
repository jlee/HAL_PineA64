; CDDL HEADER START
;
; The contents of this file are subject to the terms of the
; Common Development and Distribution License (the "Licence").
; You may not use this file except in compliance with the Licence.
;
; You can obtain a copy of the licence at
; RiscOS/Sources/HAL/HAL_PineA64/LICENCE.
; See the Licence for the specific language governing permissions
; and limitations under the Licence.
;
; When distributing Covered Code, include this CDDL HEADER in each
; file and include the Licence file. If applicable, add the
; following below this CDDL HEADER, with the fields enclosed by
; brackets "[]" replaced with your own identifying information:
; Portions Copyright [yyyy] [name of copyright owner]
;
; CDDL HEADER END
;
;
; Portions Copyright 2019 Michael Grunditz
; Portions Copyright 2019 John Ballance
; Use is subject to license terms.
;
;



        AREA    |ARM$$code|, CODE, READONLY

        GET     Hdr:ListOpts
        GET     Hdr:Macros
        GET     Hdr:Proc
        GET     Hdr:System
        GET     Hdr:OSEntries
        GET     Hdr:HALEntries
        GET     PINEA64.hdr
        GET     StaticWS.hdr
        GET     Audio.hdr

        IMPORT  memcpy
 [ Debug
        IMPORT  DebugHALPrint
        IMPORT  DebugHALPrintReg
 ]

        EXPORT  Audio_InitDevices

; Sample rate table

        MACRO
        samplerate $freq
        DCD     $freq*1024              ; frequency value as reported by Sound_SampleRate
        DCB     (1000000+($freq/2))/$freq ; period as reported via Sound_Configure
        %       3                       ; padding to 8 bytes
        MEND

        ASSERT  AudioRateTableSize = 8

Audio_RateTable
        samplerate 8000
        samplerate 11025
        samplerate 12000
        samplerate 16000
        samplerate 22050
        samplerate 24000
        samplerate 32000
        samplerate 44100
        samplerate 48000
        samplerate 96000
        samplerate 192000
Audio_NumRates * (. - Audio_RateTable) / AudioRateTableSize

; Template for audio device

AudioTemplate
        DCW     HALDeviceType_Audio + HALDeviceAudio_AudC
        DCW     HALDeviceID_AudC_SGTL5000 ; XXX HACK
        DCD     HALDeviceBus_Peri + HALDevicePeriBus_APB
        DCD     &10000 ; 1.0
        DCD     Audio_Description
        DCD     0 ; address: filled in later
        %       12
        DCD     Audio_Activate
        DCD     Audio_Deactivate
        DCD     Audio_Reset
        DCD     Audio_Sleep
      [ JackDetect
        DCD     IRQ_AC_DET
      |
        DCD     -1 ; No IRQs used
      ]
        DCD     0
        DCD     0
        %       4
        DCD     0 ; Mixer device ptr: filled in later
        DCD     1 ; Output channels
        DCD     0 ; Input channels
        ; DMA channel parameters
        DCD     0 ; flags
        DCD     DMA_DRQ_AUDIO
        DCD     0 ; 'cycle speed'
        DCD     2 ; transfer unit size
        DCD     A64_AC+DA_TXFIFO ; physical address
        ; handler functions
        DCD     Audio_PreEnable
        DCD     Audio_PostEnable
        DCD     Audio_PreDisable
        DCD     Audio_PostDisable
      [ JackDetect
        DCD     Audio_IRQHandle
      |
        DCD     0 ; IRQ handler
      ]
        DCD     Audio_NumRates
        DCD     Audio_RateTable
        DCD     Audio_SetRate
        ASSERT  . - AudioTemplate = HALDevice_Audio_Size_1

; Template for mixer device

MixerTemplate
        DCW     HALDeviceType_Audio + HALDeviceAudio_Mixer
        DCW     HALDeviceID_Mixer_SGTL5000 ; XXX HACK
        DCD     HALDeviceBus_Peri + HALDevicePeriBus_APB
        DCD     1 ; 0.1
        DCD     Mixer_Description
        DCD     0 ; address: filled in later
        %       12
        DCD     Mixer_Activate
        DCD     Mixer_Deactivate
        DCD     Mixer_Reset
        DCD     Mixer_Sleep
        DCD     -1 ; No IRQs used
        DCD     0
        DCD     0
        %       4
        DCD     0 ; Audio device ptr: filled in later
        DCD     Mixer_Channels
        DCD     Mixer_GetFeatures
        DCD     Mixer_SetMix
        DCD     Mixer_GetMix
        DCD     Mixer_GetMixLimits
        ASSERT  . - MixerTemplate = HALDevice_Mixer_Size_0_1

Audio_Description
        = "Allwinner A64 Audio Controller", 0

Mixer_Description
        = "Allwinner A64 Audio Mixer", 0
        ALIGN

Audio_InitDevices ROUT
        Entry   "v1-v5"
        ; Map in memory
	MOV     a1, #0
        LDR     a2, =A64_AC
        MOV     a3, #A64_AC_SIZE
        CallOS  OS_MapInIO
        MOV     v1, a1
        DebugReg v1, "Audio regs "
        ; Set up audio device
        ADR     a1, AudioDevice
        ADR     a2, AudioTemplate
        MOV     a3, #?AudioDevice
        BL      memcpy
        STR     v1, AudioDevice+HALDevice_Address
        ; Set up mixer device
        ADR     a1, MixerDevice
        ADR     a2, MixerTemplate
        MOV     a3, #?MixerDevice
        BL      memcpy
        STR     v1, MixerDevice+HALDevice_Address
        ADR     v2, AudioDevice
        ADR     v3, MixerDevice
        DebugReg v2, "Audio device "
        DebugReg v3, "Mixer device "
        STR     v2, [v3, #HALDevice_MixerCtrlr]
        STR     v3, [v2, #HALDevice_AudioMixer]
        ; Register devices
        MOV     a1, #0
        MOV     a2, v2
        CallOS  OS_AddDevice
        MOV     a1, #0
        MOV     a2, v3
        CallOS  OS_AddDevice
        EXIT

pll_24mhz * 24576000
pll_ctrl_24mhz * &80035514
pll_22mhz * 22579200
pll_ctrl_22mhz * &80034e14

Audio_Activate ROUT
        Entry   "sb"
        SUB     sb, a1, #:INDEX:AudioDevice
        ; Configure speaker power/reset GPIO (port H pin 7)
        LDR     a2, PC_Log
        LDR     a3, [a2, #PH_CFG0]
        BIC     a3, a3, #&F0000000
        ORR     a3, a3, #&10000000
        STR     a3, [a2, #PH_CFG0]
        BL      SpeakerOff
        MOV     a1, #1
        STRB    a1, AudioActive
        EXIT

SpeakerOff ROUT
        LDR     a2, PC_Log
        LDRB    a3, [a2, #PH_DAT]
        BIC     a3, a3, #128
        STRB    a3, [a2, #PH_DAT]
        MOV     pc, lr

; In: a2 = CCU_Log
;     a3 = sample rate index
; Out: All regs preserved
SetPLL ROUT
        Entry
        CMP     a3, #1 ; 11025
        CMPNE   a3, #4 ; 22050
        CMPNE   a3, #7 ; 44100
        LDREQ   lr, =pll_22mhz ; ~22MHz needed for CD rates
        LDRNE   lr, =pll_24mhz ; ~24MHz for all others
        STR     lr, AudioPLLRate
        LDREQ   lr, =pll_ctrl_22mhz
        LDRNE   lr, =pll_ctrl_24mhz
        STR     lr, [a2, #PLL_AUDIO_CTRL_REG]
        ; Wait for lock
10
        LDR     lr, [a2, #PLL_AUDIO_CTRL_REG]
        TST     lr, #1:SHL:28
        BEQ     %BT10
        EXIT

Audio_Deactivate ROUT
Audio_Reset
        Entry   "sb"
        SUB     sb, a1, #:INDEX:AudioDevice
        MOV     a3, #0
        STRB    a3, AudioActive
        LDRB    a3, AudioOn
        CMP     a3, #0
        BLNE    Audio_PreDisable ; Turn speaker & headphone off, if not already
        BL      Audio_PostDisable ; Shut everything else down
        EXIT

DisableI2S ROUT
        ; Disable I2S
        LDR     a2, [a1, #HALDevice_Address]
        MOV     a3, #0
        STR     a3, [a2, #DA_CTL]
        STR     a3, [a2, #DA_INT]
        MOV     pc, lr

DisableModule ROUT
        ; Disable module
        LDR     a2, [a1, #HALDevice_Address]
        MOV     a3, #0
        STR     a3, [a2, #MOD_RST_CTL]
        STR     a3, [a2, #MOD_CLK_ENA]
        STR     a3, [a2, #SYSCLK_CTL]
        MOV     pc, lr

DisableCCU ROUT
        ; Disable clocks + PLL in CCU
        LDR     a2, CCU_Log
        MOV     a3, #0
        STR     a3, [a2, #AC_DIG_CLK_REG]
        LDR     a3, [a2, #BUS_CLK_GATING_REG2]
        BIC     a3, a3, #1
        STR     a3, [a2, #BUS_CLK_GATING_REG2]
        LDR     a3, [a2, #PLL_AUDIO_CTRL_REG]
        BIC     a3, a3, #1:SHL:31
        STR     a3, [a2, #PLL_AUDIO_CTRL_REG]
        LDR     a3, [a2, #BUS_SOFT_RST_REG3]
        BIC     a3, a3, #1
        STR     a3, [a2, #BUS_SOFT_RST_REG3]
        MOV     pc, lr

Audio_Sleep ROUT
Mixer_Sleep
        ; Could probably do something here
        MOV     a1, #0
        MOV     pc, lr

; In: a2 = DMA buffer length
Audio_PreEnable ROUT
        MOV     pc, lr

OVERSAMPLE * 128 ; Magic number

; In: a2 = DMA buffer length
Audio_PostEnable ROUT
        Entry   "v1-v3,sb"
        SUB     sb, a1, #:INDEX:AudioDevice
        ; Bring it out of reset
        LDR     a2, CCU_Log
        LDR     a3, [a2, #BUS_SOFT_RST_REG3]
        ORR     a3, a3, #1
        STR     a3, [a2, #BUS_SOFT_RST_REG3]
        LDRB    a3, AudioRate
        BL      SetPLL
        ; Enable digital clocks
        LDR     a4, [a2, #BUS_CLK_GATING_REG2]
        ORR     a4, a4, #1
        STR     a4, [a2, #BUS_CLK_GATING_REG2]
        MOV     a4, #1:SHL:31
        STR     a4, [a2, #AC_DIG_CLK_REG]

        LDR     a2, [a1, #HALDevice_Address]

        ; Enable more clocks
        LDR     a4, =&B38
        STR     a4, [a2, #SYSCLK_CTL]
        LDR     a4, =&8004
        STR     a4, [a2, #MOD_CLK_ENA]
        STR     a4, [a2, #MOD_RST_CTL]

        ; Configure I2S
        MOV     a4, #&C
        STR     a4, [a2, #DA_FAT0]
        MOV     a4, #1
        STR     a4, [a2, #DA_TXCHSEL]
        LDR     a4, =&3210
        STR     a4, [a2, #DA_TXCHMAP]
        LDR     a4, =&400F4
        STR     a4, [a2, #DA_FCTL]
        ; Calculate MCLKDIV
        LDR     a4, AudioPLLRate
        ADR     v1, Audio_RateTable
        ASSERT  AudioRateTable_Frequency = 0
        LDR     v1, [v1, a3, LSL #3]
        ASSERT  OVERSAMPLE = 128
        MOV     v1, v1, LSR #10 - 7 ; /1024 to get Hz, *OVERSAMPLE
        UDIV    v1, a4, v1
        ADR     a4, MCLKDIVTab
        BL      TableLookup
        ORR     v1, v1, #&80
        ; OVERSAMPLE = 128 and word size = 32, so BCLKDIV = 2 (field value 0)
        STR     v1, [a2, #DA_CLKD]

        ; Configure AIF
        ORR     a4, a3, a3, LSL #4
        MOV     a4, a4, LSL #8
        STR     a4, [a2, #SYS_SR_CTRL]
        LDR     a4, AudioPLLRate
        ADR     v1, Audio_RateTable
        ASSERT  AudioRateTable_Frequency = 0
        LDR     v1, [v1, a3, LSL #3]
        ; BCLK1/LRCK = 64
        MOV     v1, v1, LSR #10 - 6
        UDIV    v1, a4, v1
        ADR     a4, AIFCLKDIVTab
        BL      TableLookup
        MOV     v1, v1, LSL #9
        ORR     v1, v1, #(2<<6)+16
        ORR     v1, v1, #1<<15
        STR     v1, [a2, #AIF1_CLK_CTRL]
        MOV     v1, #3<<14
        STR     v1, [a2, #AIF1_DACDAT_CTRL]

        ; Enable DAC
        BL      SetDACVol
        MOV     v1, #&8800
        STR     v1, [a2, #DAC_MXR_SRC]
        MOV     v1, #&8000
        STR     v1, [a2, #DAC_DIG_CTRL]

      [ JackDetect
        PHPSEI  v3
        ; Enable jack detection
        LDR     v1, =&F38
        STR     v1, [a2, #HMIC_CTRL1]
        LDR     v1, =&1400
        STR     v1, [a2, #HMIC_CTRL2]
        ; Zap any existing IRQs
        MOV     v1, #&19
        STR     v1, [a2, #HMIC_STS]
        MOV     v1, #&F2
        MOV     v2, #ANA_JM_DET_CTRL
        BL      AnaWrite
        ; Detect initial state
        ; TODO: Delay to make sure it's correct
        LDR     v1, [a2, #HMIC_STS]
        ANDS    v1, v1, #&1F00
        MOVNE   v1, #1
        STRB    v1, JackState
      ]

        ; Configure analogue section
        MOV     v1, #2
        MOV     v2, #ANA_OL_MIX_CTRL
        BL      AnaWrite
        MOV     v2, #ANA_OR_MIX_CTRL
        BL      AnaWrite
        MOV     v1, #&FF
        MOV     v2, #ANA_MIX_DAC_CTRL
        BL      AnaWrite
        BL      SetHPVol
        BL      SetLineOutVol

        ; Enable IS2
        LDR     a4, =&105
        STR     a4, [a2, #DA_CTL]
        MOV     a4, #&80
        STR     a4, [a2, #DA_INT]

        MOV     a4, #1
        STRB    a4, AudioOn

      [ JackDetect
        PLP     v3
      ]

        EXIT

MCLKDIVTab
        DCB     1, 2, 4, 6, 8, 12, 16, 24, 32, 48, 64, 0
        ALIGN

AIFCLKDIVTab
        DCB     1, 2, 4, 6, 8, 12, 16, 24, 32, 48, 64, 96, 128, 192, 0
        ALIGN

; In: a4 -> table
;     v1 = value
; Out: v1 = table index
TableLookup ROUT
        Entry   "a4"
10
        LDRB    lr, [a4], #1
        CMP     lr, #0
        CMPNE   v1, lr
        BHI     %BT10
        SUB     v1, a4, #1
        PullEnv
        SUB     v1, v1, a4
        MOV     pc, lr

SetDACVol ROUT
        Entry   "v1"
        LDR     v1, MixerSettings + MixerChannel_System*8
        CMP     v1, #0
        MOVNE   v1, #0
        BNE     %FT10
        LDR     v1, MixerSettings + MixerChannel_System*8 + 4
        MOV     lr, #12
        SDIV    v1, v1, lr
        ADD     v1, v1, #&A0
        ORR     v1, v1, v1, LSL #8
10
        LDR     lr, [a1, #HALDevice_Address]
        STR     v1, [lr, #DAC_VOL_CTRL]
        EXIT

SetHPVol ROUT
      [ JackDetect
        Entry   "v1-v3"
        PHPSEI  v3
      |
        Entry   "v1-v2"
      ]
        LDR     v1, MixerSettings + MixerChannel_Headphones*8
      [ JackDetect
        LDRB    v2, JackState
        CMP     v2, #0
        MOVNE   v1, #1 ; Jack is out, mute headphones to save power?
      ]
        CMP     v1, #0
        MOVNE   v1, #0
        BNE     %FT10
        LDR     v1, MixerSettings + MixerChannel_Headphones*8 + 4
        MOV     v1, v1, ASR #4
        ADD     v1, v1, #62 + 64
10
        MOV     v2, #ANA_HP_CTRL
        BL      AnaWrite
      [ JackDetect
        PLP     v3
      ]
        EXIT

SetLineOutVol ROUT
      [ JackDetect
        Entry   "v1-v3"
        PHPSEI  v3
      |
        Entry   "v1-v2"
      ]
        LDR     v1, MixerSettings + MixerChannel_Speaker*8
      [ JackDetect
        LDRB    v2, JackState
        CMP     v2, #0
        MOVEQ   v1, #1 ; Jack is in, mute speaker
      ]
        CMP     v1, #0
        MOVNE   v1, #0
        BNE     %FT10
        LDR     v1, MixerSettings + MixerChannel_Speaker*8 + 4
        MOV     lr, #24
        SDIV    v1, v1, lr
        ADD     v1, v1, #&1F
        MOV     v2, #ANA_LINEOUT_CTRL1
        BL      AnaWrite
        MOV     v1, #&c0
10
        MOV     v2, #ANA_LINEOUT_CTRL0
        BL      AnaWrite
        CMP     v1, #0
        LDR     v2, PC_Log
        LDR     lr, [v2, #PH_DAT]
        BICEQ   lr, lr, #128
        ORRNE   lr, lr, #128
        STR     lr, [v2, #PH_DAT]
      [ JackDetect
        PLP     v3
      ]
        EXIT

; In: v1 = value
;     v2 = register
AnaWrite ROUT
        Entry   "a1-a2"
        LDR     a1, PRCM_Log
        PHPSEI
        ; TODO simplify
        LDR     a2, [a1, #AC_PR_CFG_REG]
        ORR     a2, a2, #1<<28
        STR     a2, [a1, #AC_PR_CFG_REG]
        LDR     a2, [a1, #AC_PR_CFG_REG]
        BFI     a2, v2, #16, #5
        STR     a2, [a1, #AC_PR_CFG_REG]
        LDR     a2, [a1, #AC_PR_CFG_REG]
        BFI     a2, v1, #8, #8
        STR     a2, [a1, #AC_PR_CFG_REG]
        LDR     a2, [a1, #AC_PR_CFG_REG]
        ORR     a2, a2, #1<<24
        STR     a2, [a1, #AC_PR_CFG_REG]
        LDR     a2, [a1, #AC_PR_CFG_REG]
        BIC     a2, a2, #1<<24
        STR     a2, [a1, #AC_PR_CFG_REG]
        PLP
        EXIT

Audio_PreDisable ROUT
        Entry   "v1-v2,sb"
        SUB     sb, a1, #:INDEX:AudioDevice
        MOV     a4, #0
        STRB    a4, AudioOn
        BL      SpeakerOff
        ; Shut down headphone to avoid pops when the power goes off(/on)
        MOV     v1, #0
        MOV     v2, #ANA_HP_CTRL
        BL      AnaWrite
        EXIT

Audio_PostDisable ROUT
        Entry   "sb"
        SUB     sb, a1, #:INDEX:AudioDevice
        ; Shut everything down to save power
        BL      DisableI2S
        BL      DisableModule
        BL      DisableCCU
        EXIT

 [ JackDetect
Audio_IRQHandle ROUT
        Entry   "v1-v2,sb"
        SUB     sb, a1, #:INDEX:AudioDevice
        LDR     a2, [a1, #HALDevice_Address]
        LDR     a3, [a2, #HMIC_STS]
        LDRB    a4, AudioOn
        CMP     a4, #0
        BEQ     %FT90
        LDRB    a4, JackState
        MOV     v1, a4
        ; Any IRQs for us?
        TST     a3, #16 ; Jack in (note manual is wrong for these two bits)
        MOVNE   a4, #0
        TST     a3, #8 ; Jack out
        MOVNE   a4, #1
        AND     a3, a3, #&19
        STR     a3, [a2, #HMIC_STS]
        CMP     v1, a4
        MOVEQ   a1, #0
        EXIT    EQ
        ; State has changed
        STRB    a4, JackState
        BL      SetHPVol
        BL      SetLineOutVol
        MOV     a1, #0
        EXIT
90
        ; Audio is off, ignore IRQ
        AND     a3, a3, #&19
        STR     a3, [a2, #HMIC_STS]
        MOV     a1, #0
        EXIT
 ]

; In: a2 = sample rate index (0-based)
Audio_SetRate ROUT
        STRB    a2, [a1, #:INDEX:AudioRate-:INDEX:AudioDevice]
        MOV     pc, lr

; Activate/Deactivate/Reset handled by audio controller
Mixer_Activate ROUT
        MOV     a1, #1
        MOV     pc, lr

Mixer_Deactivate ROUT
Mixer_Reset
        MOV     pc, lr

; In: a2 = channel number
; Out: flags + category
Mixer_GetFeatures ROUT
        ADR     a1, Mixer_FeaturesTab
        LDR     a1, [a1, a2, LSL #2]
        MOV     pc, lr

Mixer_FeaturesTab
        ; System audio
        DCW     0
        DCW     MixerCategory_System
        ; Speakers
        DCW     0
        DCW     MixerCategory_Speaker
        ; Headphones
        DCW     0
        DCW     MixerCategory_Headphones

; In: a2 = channel
;     a3 = mute flag
;     a4 = gain, in dB*16
Mixer_SetMix ROUT
        Entry   "sb"
        SUB    sb, a1, #:INDEX:MixerDevice
        ADRL   ip, MixerSettings
        ADD    ip, ip, a2, LSL #3
        STMIA  ip, {a3-a4}
        ; Apply changes if audio is on
        LDRB   ip, AudioOn
        CMP    ip, #0
        MOV    lr, pc
        LDRNE  pc, [pc, a2, LSL #2]
        EXIT
        ASSERT MixerChannel_System = 0
        DCD    SetDACVol
        ASSERT MixerChannel_Speaker = 1
        DCD    SetLineOutVol
        ASSERT MixerChannel_Headphones = 2
        DCD    SetHPVol

; In: a2 = channel
; Out: a1 = mute flag
;      a2 = gain
Mixer_GetMix ROUT
        ADD     a1, a1, #:INDEX:MixerSettings-:INDEX:MixerDevice
        ADD     a1, a1, a2, LSL #3
        LDMIA   a1, {a1, a2}
        MOV     pc, lr

; In: a2 = channel
;     a3 -> buffer
; Out: buffer filled with dB min, max, step values
Mixer_GetMixLimits
        ADR     a1, Mixer_LimitsTab
        ADD     a1, a1, a2, LSL #3
        ADD     a1, a1, a2, LSL #2
        LDMIA   a1, {a1-a2,a4}
        STMIA   a3, {a1-a2,a4}
        MOV     pc, lr

Mixer_LimitsTab
        ; System audio
        ; AIF1_VOL_CTRL3 is -119.25 to +71.25 in 0.75dB steps
        ; There are other volume controls too, but -119 to +72 should be plenty
        DCD     -119*16 - 4
        DCD     71*16 + 4
        DCD     12
        ; Speakers
        ; LINEOUT_CTRL1, -43.5dB to 0dB, in 1.5dB steps
        DCD     -43*16 - 8
        DCD     0
        DCD     16 + 8
        ; Headphones
        ; HP_CTRL HPVOL field, -62dB to 0dB in 1dB steps
        DCD     -62*16
        DCD     0
        DCD     16

        END

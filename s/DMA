; CDDL HEADER START
;
; The contents of this file are subject to the terms of the
; Common Development and Distribution License (the "Licence").
; You may not use this file except in compliance with the Licence.
;
; You can obtain a copy of the licence at
; RiscOS/Sources/HAL/HAL_PineA64/LICENCE.
; See the Licence for the specific language governing permissions
; and limitations under the Licence.
;
; When distributing Covered Code, include this CDDL HEADER in each
; file and include the Licence file. If applicable, add the
; following below this CDDL HEADER, with the fields enclosed by
; brackets "[]" replaced with your own identifying information:
; Portions Copyright [yyyy] [name of copyright owner]
;
; CDDL HEADER END
;
;
; Portions Copyright 2019 Michael Grunditz
; Portions Copyright 2019 John Ballance
; Use is subject to license terms.
;
;
;
;

        AREA    |ARM$$code|, CODE, READONLY

        GET     Hdr:ListOpts
        GET     Hdr:Macros
        GET     Hdr:Proc
        GET     Hdr:System
        GET     Hdr:OSEntries
        GET     Hdr:HALEntries
        GET     PINEA64.hdr
        GET     StaticWS.hdr

        IMPORT  memcpy
 [ Debug
        IMPORT  DebugHALPrint
        IMPORT  DebugHALPrintReg
 ]
        IMPORT  HAL_IRQClear

        GBLL    AlwaysIRQ
AlwaysIRQ SETL  {FALSE}

        GBLL    DMADebug
DMADebug SETL   {FALSE} :LAND: Debug

        EXPORT  DMA_InitDevices
; Template for DMA controller

DMACTemplate
        DCW     HALDeviceType_SysPeri + HALDeviceSysPeri_DMAC
        DCW     HALDeviceID_DMAC_IMX6 ; XXX HACK
        DCD     HALDeviceBus_Sys + HALDeviceSysBus_AHB
        DCD     &10000 ; 1.0
        DCD     DMAC_Description
        DCD     0 ; address: filled in later
        %       12
        DCD     DMAC_Activate
        DCD     DMAC_Deactivate
        DCD     DMAC_Reset
        DCD     DMAC_Sleep
        DCD     IRQ_DMA ; Device. Bit 31 not set, because DMAManager should either use the controller to determine the interrupt cause, or the channels, but not both.
        DCD     0
        DCD     0
        %       4
        DCD     DMAC_Features
        DCD     DMAC_Enumerate
        DCD     DMAC_Allocate
        DCD     DMAC_Deallocate
        DCD     DMAC_TestIRQ2
        ASSERT  . - DMACTemplate = HALDevice_DMAC_Size_0_1

; Template for DMA channels

DMALTemplate
        DCW     HALDeviceType_SysPeri + HALDeviceSysPeri_DMAL
        DCW     HALDeviceID_DMAL_IMX6 ; XXX HACK
        DCD     HALDeviceBus_Sys + HALDeviceSysBus_AHB
        DCD     &10000 ; 1.0
        DCD     0 ; description: filled in later
        DCD     0 ; address: filled in later
        %       12
        DCD     DMAL_Activate
        DCD     DMAL_Deactivate
        DCD     DMAL_Reset
        DCD     DMAL_Sleep
        DCD     IRQ_DMA+(1<<31) ; Device. Bit 31 set because it's shared with all the channels.
        DCD     DMAL_TestIRQ
        DCD     0
        %       4
        DCD     DMAL_Features
        DCD     0 ; controller: filled in later
        DCD     DMAL_Abort
        DCD     DMAL_SetOptions
        DCD     DMAL_SetListTransfer
        DCD     DMAL_ListTransferProgress
        DCD     DMAL_ListTransferStatus
        DCD     DMAL_CurtailListTransfer
        ASSERT  . - DMALTemplate = HALDevice_DMAL_Size

DMAC_Description
        = "Allwinner A64 DMA controller", 0

DMAL_Description
        = "Allwinner A64 DMA channel ", 0
        ASSERT (. + 1) - DMAL_Description <= ?DMACDesc
        ALIGN

DMA_InitDevices ROUT
        Entry   "v1-v5"
        LDR     a1, =(1<<DMA_CH_count)-1
        STR     a1, DMAFreeChannels
        ; Map in memory
	MOV     a1, #0
        LDR     a2, =A64_DMA
        MOV     a3, #A64_DMA_64
        CallOS  OS_MapInIO
        MOV     v1, a1
      [ DMADebug
        DebugReg v1, "DMA regs "
      ]
        MOV     a1, #0
        STR     a1, [v1, #DMA_IRQ_EN_REG]
        ; Set up controller
        ADR     a1, DMAController
        ADR     a2, DMACTemplate
        MOV     a3, #?DMAController
        BL      memcpy
        STR     v1, DMAController+HALDevice_Address
        ; Set up channels
        ADRL    v2, DMAChannels
        MOV     v3, #0
        ADR     v4, DMAController
10
        MOV     a1, v2
        ADR     a2, DMALTemplate
        MOV     a3, #HALDevice_DMAL_Size
        BL      memcpy
        ADD     a1, v2, #:INDEX:DMACDesc
        STR     a1, [v2, #HALDevice_Description]
        ADR     a2, DMAL_Description
20
        LDRB    a3, [a2], #1
        CMP     a3, #0
        ADDEQ   a2, v3, #'0'
        STREQB  a2, [a1], #1
        STRB    a3, [a1], #1
        BNE     %BT20
        STR     v1, [v2, #HALDevice_Address]
        STR     v4, [v2, #HALDevice_DMAController]
        STR     v3, [v2, #:INDEX:DMACChannelNumber]
        STR     sb, [v2, #:INDEX:DMACWorkspace]
        ADRL    a1, DMAChannelList
        STR     v2, [a1, v3, LSL #2]
        ; Register as we go along
      [ DMADebug
        DebugReg v2, "channel dev "
      ]
        MOV     a1, #0
        MOV     a2, v2
        CallOS  OS_AddDevice
        ADD     v2, v2, #DMAC_DeviceSize
        ADD     v3, v3, #1
        CMP     v3, #DMA_CH_count
        BNE     %BT10
        ; Register the controller
      [ DMADebug
        DebugReg v4, "Controller dev "
      ]
        MOV     a1, #0
        MOV     a2, v4
        CallOS  OS_AddDevice
        EXIT

DMAC_Activate ROUT
        Entry   "sb"
        SUB     sb, a1, #:INDEX:DMAController
      [ DMADebug
        DebugTX "-> DMAC_Activate"
      ]
        ; Perform a reset just in case we're in a funny state
        BL      DMAC_Reset ; Assume exit with a2 = HALDevice_Address
    [ AlwaysIRQ
        ; package end & queue end interrupts (queue end is redundant?)
      [ DMA_CH_count = 7
        LDR     a3, =&06666666
      |
        LDR     a3, =&66666666
      ]
        STR     a3, [a2, #DMA_IRQ_EN_REG]
    ]
        MOV     a1, #1
      [ DMADebug
        DebugTX "<- DMAC_Activate"
      ]
        EXIT

DMAC_Deactivate ROUT
DMAC_Reset
        Entry   "sb"
        SUB     sb, a1, #:INDEX:DMAController
      [ DMADebug
        DebugTX "-> DMAC_Deactivate"
      ]
        LDR     a2, [a1, #HALDevice_Address]
        ; Disable all IRQs
        MOV     a3, #0
        STR     a3, [a2, #DMA_IRQ_EN_REG]
        ; Disable all channels
        ADD     a4, a2, #DMA_CH_EN_REG
        MOV     ip, #DMA_CH_count
10
        SUBS    ip, ip, #1
        STR     a3, [a4, ip, LSL #DMA_CH_REG_shift]
        BNE     %BT10
        ; Wait for idle
20
        LDR     a3, [a2, #DMA_STA_REG]
        CMP     a3, #0
        BNE     %BT20
        ; Clear IRQs
        LDR     a3, [a2, #DMA_IRQ_PEND_REG]
        STR     a3, [a2, #DMA_IRQ_PEND_REG]
        ; n.b. exiting with a2 = HALDevice_Address for DMAC_Activate
      [ DMADebug
        DebugTX "<- DMAC_Deactivate"
      ]
        EXIT

DMAC_Sleep ROUT
DMAL_Sleep
        ; Nothing
        MOV     a1, #0
        MOV     pc, lr

; Out: features flag (zero, no features to report)
DMAC_Features ROUT
        MOV     a1, #0
        MOV     pc, lr

; Out: channel device list + channel count
DMAC_Enumerate ROUT
        ADD     a1, a1, #:INDEX:DMAChannelList-:INDEX:DMAController
        MOV     a2, #DMA_CH_count
        MOV     pc, lr

; In: a2 = DRQ
; Out: assigned channel device
DMAC_Allocate ROUT
        Entry   "sb"
        SUB     sb, a1, #:INDEX:DMAController
      [ DMADebug
        DebugTX "-> DMAC_Allocate"
        DebugReg a2, "DRQ "
      ]
        LDR     a3, DMAFreeChannels
      [ DMADebug
        DebugReg a3, "Free channels: "
      ]
        CLZ     a4, a3
        RSBS    a4, a4, #31
        MOVLT   a1, #0
        BLT     %FT90 ; No free channels
      [ DMADebug
        DebugReg a4, "chan #"
      ]
        ; Mark channel as claimed
        MOV     ip, #1
        BIC     a3, a3, ip, LSL a4
        STR     a3, DMAFreeChannels
        ; Get channel pointer
        ADD     a1, a1, a4, LSL #2
        LDR     a1, [a1, #:INDEX:DMAChannelList-:INDEX:DMAController]
        ; Remember DRQ the channel is going to service
        STR     a2, DMACDRQ
90
      [ DMADebug
        DebugReg a1, "chan ptr "
        DebugTX "<- DMAC_Allocate"
      ]
        EXIT

; In: a2 = DRQ
;     a3 = channel device pointer
DMAC_Deallocate ROUT
        Entry   "sb"
        SUB     sb, a1, #:INDEX:DMAController
      [ DMADebug
        DebugTX "-> DMAC_Deallocate"
        DebugReg a3, "chan ptr "
      ]
        LDR     a3, [a3, #:INDEX:DMACChannelNumber]
        LDR     a2, DMAFreeChannels
        MOV     ip, #1
        ORR     a2, a2, ip, LSL a3
        STR     a2, DMAFreeChannels
      [ DMADebug
        DebugTX "<- DMAC_Deallocate"
      ]
        EXIT

; Out: a1 = channel index that's interrupting, or -1 for none
DMAC_TestIRQ2 ROUT
        Entry   "sb"
        SUB     sb, a1, #:INDEX:DMAController
      [ DMADebug
        DebugTX "-> DMAC_TestIRQ2"
      ]
        LDR     a1, [a1, #HALDevice_Address]
        LDR     a2, [a1, #DMA_IRQ_EN_REG]
        LDR     a1, [a1, #DMA_IRQ_PEND_REG]
        AND     a1, a1, a2
      [ DMADebug
        DebugReg a1, "IRQ_PEND "
      ]
        CLZ     a1, a1
        RSBS    a1, a1, #31
        BLT     %FT90
        ; 4 IRQ bits per channel
        MOV     a1, a1, ASR #2
      [ DMADebug
        DebugReg a1, "= chan #"
        DebugTX "<- DMAC_TestIRQ2"
      ]
        EXIT
90
        MOV     a1, #IRQ_DMA
        BL      HAL_IRQClear
        MOV     a1, #-1
      [ DMADebug
        DebugTX "<- DMAC_TestIRQ2 chan -1"
      ]
        EXIT

; Channel has been programmed, enable it
DMAL_Activate ROUT
        Entry   "sb"
        LDR     sb, DMACWorkspace
        LDR     a2, [a1, #HALDevice_Address]
        LDR     a3, DMACChannelNumber
      [ DMADebug
        DebugReg a3,"-> DMAL_Activate #"
      ]
        ADD     ip, a2, a3, LSL #DMA_CH_REG_shift
        ; Assume no barriers required here (transfer descriptors & data shouldn't be modified after SetListTransfer)
        MOV     a4, #0
        STR     a4, [ip, #DMA_CH_PAU_REG] ; Ensure not paused
        PHPSEI
        MOV     a4, #1
        STR     a4, [ip, #DMA_CH_EN_REG] ; Enable
        STR     a4, DMACActive ; Update our status flag
      [ :LNOT: AlwaysIRQ
        ; Enable IRQs for the channel
        LDR     ip, [a2, #DMA_IRQ_EN_REG]
        MOV     a4, #2 ; &6 ; package end & queue end interrupts (queue end is redundant?)
        MOV     a3, a3, LSL #2
        ORR     ip, ip, a4, LSL a3
        STR     ip, [a2, #DMA_IRQ_EN_REG]
      ]
        PLP
        ; XXX wait here until the descriptor has been loaded, so any immediate calls to ListTransferProgress will be correct
        MOV     a1, #1
      [ DMADebug
        DebugTX "<- DMAL_Activate"
      ]
        EXIT

; Stop channel (in blocking manner)
DMAL_Deactivate ROUT
DMAL_Abort ; Stop ASAP "without blocking"
DMAL_Reset
        Entry   "a1,v1-v3,sb"
        LDR     sb, DMACWorkspace
        LDR     v1, [a1, #HALDevice_Address]
        LDR     v2, DMACChannelNumber
      [ DMADebug
        DebugReg v2,"-> DMAL_Deactivate #"
      ]
        ADD     v3, v1, v2, LSL #DMA_CH_REG_shift
        ; ListTransferProgress can get called after Abort or Deactivate have
        ; been called. But, clearing CH_EN_REG resets some of the controller
        ; registers, so to give (mostly) accurate progress information we must
        ; calculate and cache the progress before we disable the channel.
        MOV     ip, #1
        STR     ip, [v3, #DMA_CH_PAU_REG] ; Pause to increase accuracy
        BL      DMAL_ListTransferProgress
      [ DMADebug
        DebugReg a1, "Caching progress "
      ]
        FRAMLDR a2,,a1
        STR     a1, [a2, #:INDEX:DMACFinalProgress]
        ; Disable the channel completely (pausing doesn't clear busy state)
        PHPSEI
        MOV     ip, #0
        STR     ip, [v3, #DMA_CH_EN_REG]
        STR     ip, [a2, #:INDEX:DMACActive]
      [ :LNOT: AlwaysIRQ
        ; Disable IRQs for the channel (DMAManager can't cope with receiving IRQs for inactive channels)
        LDR     ip, [v1, #DMA_IRQ_EN_REG]
        MOV     a2, #&F
        MOV     a3, v2, LSL #2
        BIC     ip, ip, a2, LSL a3
        STR     ip, [v1, #DMA_IRQ_EN_REG]
        ; ... and clear any pending IRQs for good measure
        LDR     ip, [v1, #DMA_IRQ_PEND_REG]
        ANDS    ip, ip, a2, LSL a3
        STRNE   ip, [v1, #DMA_IRQ_PEND_REG]
      ]
        PLP
        ; Wait for idle status
        MOV     ip, #1
        MOV     v2, ip, LSL v2
10
        LDR     ip, [v1, #DMA_STA_REG]
        TST     ip, v2
        BNE     %BT10
      [ DMADebug
        DebugTX "<- DMAL_Deactivate"
      ]
        EXIT

DMAL_TestIRQ ROUT
        Entry   "sb"
        LDR     sb, DMACWorkspace
        LDR     a3, DMACChannelNumber
      [ DMADebug
        DebugReg a3,"-> DMAL_TestIRQ #"
      ]
        LDR     a1, [a1, #HALDevice_Address]
        LDR     a2, [a1, #DMA_IRQ_EN_REG]
        LDR     a1, [a1, #DMA_IRQ_PEND_REG]
        AND     a1, a1, a2
        ; 4 IRQ bits per channel
        MOV     a3, a3, LSL #2
        MOV     a4, #&F
        ANDS    a1, a1, a4, LSL a3
        MOVNE   a1, #1
      [ DMADebug
        DebugReg a1,"<- DMAL_TestIRQ "
      ]
        EXIT

 [ {FALSE} ; Not used by DMAManager!
DMAL_ClearIRQ ROUT
        Entry   "sb"
        LDR     sb, DMACWorkspace
        ; Clear all the IRQs for this channel?
        LDR     a3, DMACChannelNumber
      [ DMADebug
        DebugReg a3,"DMAL_ClearIRQ #"
      ]
        LDR     a1, [a1, #HALDevice_Address]
        MOV     a3, a3, LSL #2
        MOV     a4, #&F
        LDR     ip, [a1, #DMA_IRQ_PEND_REG]
        ANDS    ip, ip, a4, LSL a3
        STRNE   ip, [a1, #DMA_IRQ_PEND_REG]
        ; We are also expected to call HAL_IRQClear
        ; (IRQ should get re-raised if more channels need servicing)
        MOVNE   a1, #IRQ_DMA
        BLNE    HAL_IRQClear
        EXIT
 ]

DMAL_Features ROUT
        ADR     a1, features
        MOV     pc, lr

; Enough for 512KB of data assuming 4KB per descriptor
max_descriptor_blocks * 128

features
        DCD     0 ; flags
        DCD     max_descriptor_blocks * DMA_DESC_size
        DCD     0 ; no descriptor alignment requirement (word alignment is implicit)
        DCD     0 ; no boundary limit for descriptor block
        DCD     max_descriptor_blocks
        DCD     (1<<25)-16 ; BCNT_LEFT is 25 bits wide. Subtract max block/burst size of 16 bytes.
        DCD     0 ; no boundary limit for data

; In: a2 = flags
;          b0: 1 = RAM -> device
;              0 = device -> RAM
;          b1-5: transfer unit width (bytes)
;          b6-8: 'cycle speed' (ignored, no prioritisation system available)
;          b9-12: channel reuse delay (ignored)
;          b13: 1 = disable burst transfers
;          b14: 1 = bypass clock sync (ignored)
;          b15: 1 = circular transfer
;     a3 = peripheral physical address
DMAL_SetOptions ROUT
        ; Remember the options for later (they all get programmed into the transfer descriptors)
        STR     a2, DMACFlags
        STR     a3, DMACPeriphAddr
        MOV     pc, lr

; In: a2 = phys addr of descriptor block
;     a3 = log addr of descriptor block (pairs of (addr, len))
;     a4 = number of entries in descriptor
;     [sp] = transfer length, 0 for infinite
DMAL_SetListTransfer ROUT
        ; TODO: Support non-infinite circular transfers
        Entry   "v1-v5,sb,v7"
        LDR     sb, DMACWorkspace
        LDR     v1, [sp, #8*4]
      [ DMADebug
        LDR     v2, DMACChannelNumber
        DebugReg v2,"-> DMAL_SetListTransfer #"
        DebugReg a2,"scatter phys "
        DebugReg a3,"scatter log "
        DebugReg a4,"scatter count "
        DebugReg v1,"xfer len "
        ADD     v2, a3, a4, LSL #3
01
        LDMIA   a3!, {v3-v4}
        DebugReg v3
        DebugReg v4
        CMP     a3, v2
        BNE     %BT01
        SUB     a3, a3, a4, LSL #3
      ]
        STR     a2, DMACDescriptorsPhys
        STR     a3, DMACDescriptorsLog
        STR     a4, DMACDescriptorCount
        STR     v1, DMACLength
        LDR     v2, DMACDRQ
        LDR     v3, DMACFlags
        ; Construct the config register value
        ORR     v2, v2, #32 ; IO mode
        ORR     v2, v2, #DMA_DRQ_SDRAM:SHL:16
        AND     ip, v3, #DMASetOptionsMask_Width
        TST     ip, #1:SHL:DMASetOptionsShift_Width ; Element size multiple of 16 bits?
        LDR     lr, =(1:SHL:9)+(1:SHL:25)
        ADDEQ   v2, v2, lr ; Increment src/dest width
        LDR     v5, =(1:SHL:6)+(1:SHL:22)
        TST     v3, #DMASetOptionsFlag_NoBursts
        ADDEQ   lr, lr, v5
        TST     ip, #3:SHL:DMASetOptionsShift_Width ; Element size multiple of 32 bits?
        ADDEQ   v2, v2, lr ; Increment src/dest width & block sizes
        TST     ip, #7:SHL:DMASetOptionsShift_Width ; Element size multiple of 64 bits?
        ADDEQ   v2, v2, lr ; Increment src/dest width & block sizes
        AND     lr, lr, v5
        TST     ip, #15:SHL:DMASetOptionsShift_Width ; Element size multiple of 128 bits?
        ADDEQ   v2, v2, lr ; Increment block sizes
        ; Rotate src/dest params into correct position
        TST     v3, #DMASetOptionsFlag_Write
        MOVNE   v2, v2, ROR #16
        ; v2 now contains the full config register value
        ; Work backwards through the descriptor array to avoid overwriting entries
        TST     v3, #DMASetOptionsFlag_Circular
        MOVNE   v4, a2
        LDREQ   v4, =DMA_DESC_terminator
        MOV     ip, #DMA_DESC_size
        MLA     a2, a4, ip, a2 ; -> phys end (+1) of descriptor list
        MLA     v5, a4, ip, a3 ; -> log end (+1) of descriptor list
        ; Calculate total length of scatter list
        MOV     v7, #0
        MOV     ip, a4
05
        LDR     lr, [a3, #4]
        ADD     a3, a3, #8
        ADD     v7, v7, lr
        SUBS    ip, ip, #1
        BNE     %BT05
      [ DMADebug
        DebugReg v7, "loop len "
      ]
        STR     v7, DMACLoopLength
        ; a3 -> log end of scatter list
        STR     v4, [v5, #DMA_DESC_LINK-DMA_DESC_size] ; Store last link ptr
        LDR     v4, DMACPeriphAddr
10
        SUB     v5, v5, #DMA_DESC_size
        SUB     a2, a2, #DMA_DESC_size ; next link ptr (phys addr of the entry we're writing now)
        LDMDB   a3!, {a4, v1} ; addr, len pair
        STR     v2, [v5, #DMA_DESC_CFG]
        STR     v1, [v5, #DMA_DESC_BCNT]
        TST     v3, #DMASetOptionsFlag_Write
        ; RAM -> device
        STRNE   a4, [v5, #DMA_DESC_SRC]
        STRNE   v4, [v5, #DMA_DESC_DEST]
        ; device -> RAM
        STREQ   v4, [v5, #DMA_DESC_SRC]
        STREQ   a4, [v5, #DMA_DESC_DEST]
        MOV     a4, #8 ; Magic wait cycle value from Linux sources
        STR     a4, [v5, #DMA_DESC_PARA]
        SUB     v7, v7, v1
        STR     v7, [v5, #DMA_DESC_total]
        ; scatter list ptr & descriptor ptr will match once we reach the end
        CMP     v5, a3
        STRNE   a2, [v5, #DMA_DESC_LINK-DMA_DESC_size] ; not the end, so fill in the link ptr for the previous descriptor
        BNE     %BT10
        MOV     ip, #0
        STR     ip, DMACLastLoopCount
      [ DMADebug
        LDR     a4, DMACDescriptorCount
        DebugTX "Descriptors:"
90
        LDR     v1, [a3, #DMA_DESC_CFG]
        DebugReg v1, "CFG  "
        LDR     v1, [a3, #DMA_DESC_SRC]
        DebugReg v1, "SRC  "
        LDR     v1, [a3, #DMA_DESC_DEST]
        DebugReg v1, "DEST "
        LDR     v1, [a3, #DMA_DESC_BCNT]
        DebugReg v1, "BCNT "
        LDR     v1, [a3, #DMA_DESC_PARA]
        DebugReg v1, "PARA "
        LDR     v1, [a3, #DMA_DESC_LINK]
        DebugReg v1, "LINK "
        LDR     v1, [a3, #DMA_DESC_total]
        DebugReg v1, "tot  "
        ADD     a3, a3, #DMA_DESC_size
        SUBS    a4, a4, #1
        BNE     %BT90
      ]
        ; Program the controller now, to simplify pause/resume handling
        DMB     ST ; Just in case it preloads the descriptor
        LDR     a4, [a1, #HALDevice_Address]
        LDR     v1, DMACChannelNumber
        ADD     ip, a4, v1, LSL #DMA_CH_REG_shift
        STR     a2, [ip, #DMA_CH_DESC_ADDR_REG]
      [ DMADebug
        DebugTX "<- DMAL_SetListTransfer"
      ]
        EXIT

; Out: (underestimate) of transferred data
DMAL_ListTransferProgress ROUT
        Entry   "v1-v3,sb"
        LDR     sb, DMACWorkspace
        ; For infinite circular transfers, we've got to deal with the fact that
        ; our counters will eventually wrap around. Specifically, we can't use
        ; DMA_CH_PKG_NUM_REG to directly calculate which package/descriptor
        ; we're on (or how many times we've looped) because DMACDescritporCount
        ; may not be an integer divisor of 2^32. So to counter this, we store
        ; the last loop count value, from which we can correctly calculate
        ; everything else we need to generate the result (mod 2^32)
        LDR     a2, [a1, #HALDevice_Address]
        LDR     a3, DMACChannelNumber
      [ DMADebug
        DebugReg a3,"-> DMAL_ListTransferProgress #"
      ]
      [ {TRUE}
        ; Clear any IRQs. TODO - Make DMAManager use the ClearIRQ entry point.
        MOV     v1, a3, LSL #2
        MOV     a4, #&F
        LDR     ip, [a2, #DMA_IRQ_PEND_REG]
        ANDS    lr, ip, a4, LSL v1
        BEQ     %FT05
        STR     lr, [a2, #DMA_IRQ_PEND_REG]
        ; We are also expected to call HAL_IRQClear
        ; If there are other channels that need servicing, we rely in the
        ; interrupt controller to raise the interrupt again (DMAManager only
        ; services one channel per interrupt)
        Push    "a1-a3"
        MOV     a1, #IRQ_DMA
        BL      HAL_IRQClear
        Pull    "a1-a3"
05
      ]

        ADD     a3, a2, a3, LSL #DMA_CH_REG_shift
      [ DMADebug
        ; Display the key channel registers
        LDR     a4, [a2, #DMA_STA_REG]
        LDR     v1, [a3, #DMA_CH_EN_REG]
        DebugReg a4, "STA   "
        DebugReg v1, "EN    "
        LDR     lr, [a3, #DMA_CH_DESC_ADDR_REG]
        LDR     a4, [a3, #DMA_CH_CUR_SRC_REG]
        LDR     v1, [a3, #DMA_CH_CUR_DEST_REG]
        LDR     v2, [a3, #DMA_CH_BCNT_LEFT_REG]
        LDR     v3, [a3, #DMA_CH_FDESC_ADDR_REG]
        LDR     ip, [a3, #DMA_CH_PKG_NUM_REG]
        DebugReg lr, "DESC  "
        DebugReg a4, "SRC   "
        DebugReg v1, "DEST  "
        DebugReg v2, "BCNT  "
        DebugReg v3, "FDESC "
        DebugReg ip, "PKG   "
      ]

        ; IRQs off for state read & DMACLastLoopCount update to ensure state update is consistent
        PHPSEI
        ; PKG_NUM_REG gets reset to zero when we disable the channel - so if
        ; the channel is disabled, fall back on a progress value we read
        ; earlier.
        ; However, EN_REG also gets reset to zero by the hardware once the
        ; transfer finishes - so check a flag of our own to decide whether the
        ; channel is expected to be active or not
        LDR     a4, DMACActive
        CMP     a4, #0
        BEQ     %FT90
        ; Try and get a snapshot of the current state: DMA_CH_PKG_NUM_REG, DMA_CH_BCNT_LEFT_REG, DMA_CH_EN_REG
        LDR     a4, [a3, #DMA_CH_PKG_NUM_REG]
10
        LDR     ip, [a3, #DMA_CH_BCNT_LEFT_REG]
        LDR     v2, [a3, #DMA_CH_EN_REG]
        LDR     v1, [a3, #DMA_CH_PKG_NUM_REG]
        CMP     a4, v1
        MOVNE   a4, v1
        BNE     %BT10
      [ DMADebug
        Push    "lr"
        DebugReg v2, "Measured EN_REG "
        Pull    "lr"
      ]
        ; Subtract one from PKG_NUM if the transfer has finished (otherwise we get confused and add in the length of the next descriptor)
        TST     v2, #1
        SUBEQ   a4, a4, #1
        ; Based on DMACLastLoopCount, calculate how many packages have been consumed since the start of that loop
        LDR     v1, DMACLastLoopCount
        LDR     v2, DMACDescriptorCount
        CMP     v2, #0 ; We haven't programmed anything yet? Just return the cached value (which should also be zero), to avoid division by zero
        BEQ     %FT90
        MLS     v3, v1, v2, a4 ; = DMA_CH_PKG_NUM_REG - (DMACLastLoopCount * DMACDescriptorCount)
        ; Use that to calculate how many new loops have happened, and the current package index
        DivRem  a3, v3, v2, a4
        ADD     v1, v1, a3 ; Accumulate loop count
        STR     v1, DMACLastLoopCount
        PLP

      [ DMADebug
        DebugReg v1, "New LastLoopCount "
        DebugReg v3, "cur pkg index "
        DebugReg ip, "Measured BCNT_LEFT "
      ]

        ; Calculate how many bytes correspond to that loop count
        LDR     a4, DMACLoopLength
        MUL     v1, a4, v1
        ; Add the offset for the current package
        LDR     a4, DMACDescriptorsLog
        MOV     lr, #DMA_DESC_size
        MLA     a4, v3, lr, a4
        LDR     v3, [a4, #DMA_DESC_total]
        LDR     a4, [a4, #DMA_DESC_BCNT]
        ADD     v1, v1, v3 ; amount at start of package
        ADD     v1, v1, a4 ; amount at end of package
        SUB     a1, v1, ip ; minus BCNT_LEFT
      [ DMADebug
        DebugReg a1, "<- DMAL_ListTransferProgress "
      ]
        DMB     ; For device -> RAM transfer, ensure caller sees correct buffer contents
        EXIT
90
        PLP
        LDR     a1, DMACFinalProgress
      [ DMADebug
        DebugReg a1, "<- DMAL_ListTransferProgress (cached) "
      ]
        ; No barrier needed (barrier performed when we saved DMACFinalProgress)
        EXIT

; Out: bit 0 = memory error
;      bit 1 = device error
DMAL_ListTransferStatus ROUT
        ; There are no error flags available
        MOV     a1, #0
        MOV     pc, lr

; In: a2 = minimum additional number of bytes that must be transferred before stopping
; Out: actual additional number of bytes that'll be transferred before stopping
DMAL_CurtailListTransfer ROUT
        Entry   "v1-v5,sb,v7-v8"
        LDR     sb, DMACWorkspace
        LDR     v1, [a1, #HALDevice_Address]
        LDR     v2, DMACChannelNumber
      [ DMADebug
        DebugReg v2,"-> DMAL_CurtailListTransfer #"
        DebugReg a2,"request: "
      ]
        ADD     v3, v1, v2, LSL #DMA_CH_REG_shift
        ; Pause the channel so we can safely modify the descriptor chain
        MOV     ip, #1
        STR     ip, [v3, #DMA_CH_PAU_REG]
        ; XXX do we need to wait here? (can't wait for idle status, since pausing doesn't affect it)
        LDR     a3, [v3, #DMA_CH_BCNT_LEFT_REG] ; = first valid stopping point (end of current descriptor)
      [ DMADebug
        DebugReg a3,"first stopping point: "
      ]
        SUB     a2, a2, a3 ; = number of extra bytes to transfer. Treat it as a signed value, because we could have already overshot the requested amount.
        ; Walk the descriptor chain looking for the correct stopping point
        ; We do this by actually walking the chain, just in case CurtailListTransfer has been called multiple times
        LDR     v8, [v3, #DMA_CH_FDESC_ADDR_REG]
      [ DMADebug
        DebugReg v8,"FDESC_ADDR_REG "
      ]
        LDR     v7, =DMA_DESC_terminator
        CMP     v8, v7 ; Has DMA already finished?
        BEQ     %FT90
        LDR     v5, DMACDescriptorsPhys
        LDR     a4, DMACDescriptorsLog
        SUB     v5, a4, v5
        ADD     v4, v8, v5 ; log addr of current descriptor
      [ DMADebug
        DebugReg v4, "= log addr "
      ]
20
      [ DMADebug
        DebugReg a2,"requested bytes left: "
      ]
        ; Break at end of this descriptor if necessary
        CMP     a2, #0
        BLE     %FT90
        LDR     ip, [v4, #DMA_DESC_LINK] ; phys addr of next descriptor
      [ DMADebug
        DebugReg ip, "Next phys: "
      ]
        ; Stop here if (a) there is no next descriptor, or (b) the next descriptor is the current descriptor (this implementation can't cope with looping multiple times before stopping)
        CMP     ip, v7
        CMPNE   ip, v8
        BEQ     %FT90
        ADD     v4, ip, v5
      [ DMADebug
        DebugReg v4, "= log addr "
      ]
        LDR     a4, [v4, #DMA_DESC_BCNT]
      [ DMADebug
        DebugReg a4, "Next len: "
      ]
        ; Do we need to stop in the middle of this descriptor?
        CMP     a2, a4
        SUBGE   a2, a2, a4
        ADDGE   a3, a3, a4
        BGE     %BT20
        ; Stopping somewhere in the middle of this descriptor - a2 is new BCNT value
      [ DMADebug
        DebugReg a2, "Truncating to "
      ]
        STR     a2, [v4, #DMA_DESC_BCNT]
        ADD     a3, a3, a2
90
        ; a3 = stopping point
      [ DMADebug
        DebugReg v4, "Terminating at log "
        DebugReg a3, "<- DMAL_CurtailListTransfer "
      ]
        STR     v7, [v4, #DMA_DESC_LINK] ; Break the chain at this point
        DSB     ST
        ; Resume the channel
        MOV     ip, #0
        STR     ip, [v3, #DMA_CH_PAU_REG]
        MOV     a1, a3
        EXIT

        END
